open Syntax ;;
open Cam_syntax ;;
open Eval ;;
open Util;;

let eval_file filename = 
  let decl = Iparse.main Lex.token (Lexing.from_channel (open_in filename)) in
    eval decl
;;

let eval_str str= 
  let decl = Iparse.main Lex.token (Lexing.from_string str) in
    eval decl
;;

let compile input output = 
  let decl = Iparse.main Lex.token (Lexing.from_channel (open_in input)) in
  let inss = eval decl in
    write_file inss output
;;

let _ =
  print_endline ("Input file  : " ^ Sys.argv.(1));
  print_endline ("Output file : " ^ Sys.argv.(2));
  compile Sys.argv.(1) Sys.argv.(2)
;;

