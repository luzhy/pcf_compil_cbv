%{
open Syntax
%}

%token <string> VAR  // x, y, abc, ...
%token <int> INT     // 0, 1, 2, ...

%token PLUS     // '+'
%token MINUS    // '-'
%token ASTERISK // '*'
%token SLASH    // '/'
%token EQUAL    // '='
%token SEMISEMI // ";;"
%token RETUR    // '\n' 

%token LPAREN   // '('
%token RPAREN   // ')'

%token ARROW    // "->"

%token FUN      // "fun"
%token LET      // "let"
%token IN       // "in"
%token IFZ      // "ifz"
%token THEN     // "then"
%token ELSE     // "else"
%token FIXFUN   // "fixfun"

%token EOF 

%nonassoc IN ELSE ARROW 
%left EQUAL PLUS MINUS ASTERISK SLASH

%left VAR INT LPAREN

%start main
%type <Syntax.term> main

%%

main:
  | exp SEMISEMI
    { $1 }
;

arg_exp:
  | VAR
    { Var $1 }
    
  | INT
    { Const $1 }
    
  | LPAREN exp RPAREN
    { $2 }
;

exp:
  | arg_exp
    { $1 }
    
  | exp arg_exp
    { App ($1, $2) }
  
  | exp PLUS exp
    { Bin_Op (Add, $1, $3) }
  
  | exp MINUS exp
    { Bin_Op (Sub, $1, $3) }
  
  | exp ASTERISK exp
    { Bin_Op (Mul, $1, $3) }
  
  | exp SLASH exp
    { Bin_Op (Div, $1, $3) }
    
  | FUN VAR ARROW exp
    { Func ($2, $4) }
  
  | LET VAR EQUAL exp IN exp
    { Let_In ($2, $4, $6) }
  
  | IFZ exp THEN exp ELSE exp
    { If_Zero ($2, $4, $6) }

  | FIXFUN VAR VAR ARROW exp
    { Fix_Fun ($2, $3, $5)}
  
  | EOF
    { raise Parser_EOF }

  | error
    { 
      let message =
        Printf.sprintf 
          "Parse error near characters %d-%d"
          (Parsing.symbol_start ())
	      (Parsing.symbol_end ())
	  in
	    raise (Parser_Error message)
    }
;
