
type instruction =
    | Ins_Ldi    of int
    | Ins_Push
    | Ins_Add
    | Ins_Sub
    | Ins_Mult
    | Ins_Div
    | Ins_Test   of (instruction list) * (instruction list)
    | Ins_Extend of string
    | Ins_Search of string
    | Ins_Pushenv
    | Ins_Popenv
    | Ins_Mkclos of string * string * (instruction list)
    | Ins_Apply
;;


