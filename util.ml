open Printf;;
open Cam_syntax;;

let write_file inss output =
  let handle = open_out output in
  let writei str = fprintf handle "%s" str in
  let rec iter inss = 
    let matchi ins =
      begin
        match ins with
          | Ins_Ldi(x) -> 
              writei ("Ldi" ^ " " ^ string_of_int(x))

          | Ins_Push -> 
              writei "Push"

          | Ins_Add -> 
              writei "Add"

          | Ins_Sub -> 
              writei "Sub"

          | Ins_Mult -> 
              writei "Mult"

          | Ins_Div -> 
              writei "Div"

          | Ins_Test(i, j) ->
              writei "Test";
              writei "(";
              writei "[";
              iter i ;
              writei "]";
              writei ", ";
              writei "[";
              iter j ;
              writei "]";
              writei ")"

          | Ins_Extend(x) ->
              writei ("Extend" ^ " " ^ x)

          | Ins_Search(x) -> 
              writei ("Search" ^ " " ^ x)

          | Ins_Pushenv ->
              writei "Pushenv"

          | Ins_Popenv ->
              writei "Popenv"

          | Ins_Mkclos(f, x, t) ->
              writei "Mkclos";
              writei "(";
              writei f;
              writei ", ";
              writei x;
              writei ", ";
              writei "[";
              iter t;
              writei "]";
              writei ")"

          | Ins_Apply ->
              writei "Apply"
      end
    in
      match inss with
        | [] -> ()
        | [ins_hd] -> matchi ins_hd
        | ins_hd :: ins_tl ->
            matchi ins_hd;
            writei ", ";
            iter ins_tl
  in
    iter inss
;;

