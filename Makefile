
SRC= syntax.ml cam_syntax.ml iparse.mly lex.mll eval.ml util.ml main_exe.ml main.ml 
COMPONENT= syntax.ml cam_syntax.ml iparse.mli iparse.ml lex.ml eval.ml util.ml main.ml 
TARGET= PCF_Compil_CbV

all: $(TARGET)

$(TARGET): 	$(COMPONENT)
	ocamlc    -rectypes $(COMPONENT) -o $(TARGET) 

iparse.mli:	iparse.mly
	ocamlyacc -v iparse.mly

iparse.ml:	iparse.mly
	ocamlyacc -v iparse.mly

lex.ml:	lex.mll
	ocamllex lex.mll

backup:
	/bin/cp -f Makefile $(SRC) back

clean:
	/bin/rm -f iparse.ml iparse.mli iparse.output lex.ml $(TARGET) *.cmi *.cmo *.mli

