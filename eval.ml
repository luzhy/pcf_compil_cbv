open Syntax ;;
open Cam_syntax ;;

let rec eval exp =
  match exp with
    | Var(x) -> 
        [Ins_Search(x)]

    | Bin_Op(op_type, t, u) -> 
        let ins_op = 
          match op_type with
            | Add -> Ins_Add
            | Sub -> Ins_Sub
            | Mul -> Ins_Mult
            | Div -> Ins_Div
          in
            (eval u) @ [Ins_Push] @ (eval t) @ [ins_op]

    | Const(n) -> 
        [Ins_Ldi(n)]

    | If_Zero(t, u, v) ->
        (eval t) @ [Ins_Test((eval u),(eval v))]

    | Fix_Fun(f, x, t) ->
        [Ins_Mkclos(f, x, (eval t))]

    | Func(x, t) -> 
        [Ins_Mkclos("$", x, (eval t))]

    | Let_In(x, t, u) ->
        [Ins_Pushenv] @ (eval t) @ [Ins_Extend(x)] @ 
        (eval u)   @ [Ins_Popenv]

    | App(t,u) -> 
        [Ins_Pushenv] @ (eval u) @ [Ins_Push]   @
        (eval t)   @ [Ins_Apply] @ [Ins_Popenv]

;;
